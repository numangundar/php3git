<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <h1> 1. Tentukan Nilai</h1>

    <?php
    function tentukan_nilai($number)
    {
        $nilai = $number;
        $number = "";

        if ($nilai >= 85) {
            echo "$nilai => Sangat Baik <br>";
        } elseif ($nilai < 85 && $nilai >= 70) {
            echo " $nilai=> Baik <br>";
        } elseif ($nilai < 70 && $nilai >= 60) {
            echo "$nilai => Cukup <br>";
        } else {
            echo "$nilai => Kurang <br>";
        }
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    ?>




    <h1> 2. Ubah Huruf</h1>

    <?php
    function ubah_huruf($string)
    {


        $asli = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $hurufExtend = "";

        if (!is_string($string)) {
            echo "Silahkan Masukan String";
        } else {
            foreach (str_split($string) as $str) {
                for ($i = 0; $i < count($asli); $i++) {
                    if (strtolower($str) == $asli[$i]) {
                        $hurufExtend .= $asli[$i + 1];
                    }
                }
            }
            echo $hurufExtend . "<br>";
        }
    }





    // TEST CASES
    echo ubah_huruf('wow '); // xpx
    echo ubah_huruf('developer '); // efwfmpqfs
    echo ubah_huruf('laravel '); // mbsbwfm
    echo ubah_huruf('keren '); // lfsfo
    echo ubah_huruf('semangat '); // tfnbohbu

    ?>

    <h1> 3. Tukar Besar Kecil</h1>

    <?php
    function tukar_besar_kecil($string)
    {
        $alphas = range("a", "z");
        $alphas1 = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-");
        $exchangedAlphas = "";
        foreach (str_split($string)  as $str) {

            if (ctype_lower($str)) {
                $exchangedAlphas .= strtoupper($str);
            } elseif (ctype_upper($str)) {
                $exchangedAlphas .= strtolower($str);
            }
        }

        return $exchangedAlphas . "<br>";
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

    ?>







</body>


</html>